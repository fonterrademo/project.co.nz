# Welcome to AEM in Docker #

## Installing Docker on your dev machine ##

### Getting set up with Docker on a Mac ###

Docker for Mac
Docker for Mac is our newest offering for the Mac. It runs as a native Mac application and uses xhyve to virtualize the Docker Engine environment and Linux kernel-specific features for the Docker daemon.

Go to [Getting Started with Docker for Mac](https://docs.docker.com/docker-for-mac/) for download and install instructions, and to learn all about Docker for Mac.

Requirements

* Mac must be a 2010 or newer model, with Intel’s hardware support for memory management unit (MMU) virtualization; i.e., Extended Page Tables (EPT)
* OS X 10.10.3 Yosemite or newer
* At least 4GB of RAM
* VirtualBox prior to version 4.3.30 must NOT be installed (it is incompatible with Docker for Mac). Docker for Mac will error out on install in this case. Uninstall the older version of VirtualBox and re-try the install.

### Getting set up with Docker on Windows ###

Docker for Windows is our newest offering for PCs. It runs as a native Windows application and uses Hyper-V to virtualize the Docker Engine environment and Linux kernel-specific features for the Docker daemon.

Go to [Getting Started with Docker for Windows](https://docs.docker.com/docker-for-windows/) for download and install instructions, and to learn all about Docker for Windows.

Requirements

* 64bit Windows 10 Pro, Enterprise and Education (1511 November update, Build 10586 or later). In the future we will support more versions of Windows 10.
* The Hyper-V package must be enabled. The Docker for Windows installer will enable it for you, if needed. (This requires a reboot).

## Running the AEM Docker Container ##

### Running on a MAC ###

### Running on Windows ###